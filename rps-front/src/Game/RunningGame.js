import React from "react";
import Button from '@material-ui/core/Button';
import Webcam from 'react-webcam';
import './RunningGame.css';
import Rock from "../images/rock_v2.png";
import Paper from "../images/paper_v2.png";
import Scissors from "../images/scissors_v2.png"

const labels_to_images = {
    "Rock": Rock,
    "Paper": Paper,
    "Scissors": Scissors,
}

class RunningGame extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      comment: "Rock, Paper or Scissors?",
      screenshot: null,
      computerChoiceImagePath: Rock,
      playerChoiceImagePath: Scissors,
    }
  }

  updateGame(parsedJSON){
    this.setState({comment: parsedJSON["comment"], 
                   computerChoiceImagePath: labels_to_images[parsedJSON["computer_choice"]],
                   playerChoiceImagePath: labels_to_images[parsedJSON["player_choice"]],
                })
    this.props.setScore(parsedJSON["winner"])
  }

  takeScreeshot = () => {
    const screenshot = this.webcam.getScreenshot();
    const options = {
      method: 'POST',
      body: screenshot,
    };
    fetch('http://localhost:5000/rps_api/play', options)
    .then(response => response.json())
    .then(parsedJSON => {
        this.updateGame(parsedJSON)
    })
    .catch(err => console.error(err))
  }

  setRef = (webcam) => {
    this.webcam = webcam;
  };

  render() {
    return(
      <>
       <div className="game">
        <p className="comment">{this.state.comment}</p>
        <div className="playerChoices">
            <div className="playerChoice">
                <div className="playerTopbar">
                    <Button style={{margin: "10px 20px", height: "50px"}} variant="contained" onClick={this.takeScreeshot}>Take a screenshot</Button>
                    <img className="playerImage" src={this.state.playerChoiceImagePath} alt="playerImage"/>
                </div>
                <Webcam height = {200 + '%'} width = {100 + '%'} audio ={false} ref={this.setRef} screenshotFormat="image/png"/>
            </div>
            <div className="computerChoice">
                <img className="computerImage" src={this.state.computerChoiceImagePath} alt="computerImage"/>
            </div>
        </div>
        <Button className="stopButton" variant="contained" onClick={this.props.stopGame}>Stop</Button>
       </div>
      </>
    )
  }
}

export default RunningGame;
import React from "react";
import './App.css';
import Button from '@material-ui/core/Button';
import Score from "./Game/Score";
import RunningGame from "./Game/RunningGame";

class App extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      player_score: 0,
      computer_score: 0,
      started: false
    }
  }

  startGame = () => {
    this.setState({started: true});
  }

  stopGame = () => {
    this.setState({started: false});
    this.resetScore()
  }

  setScore = (player) => {
    if(player === 0){
      this.setState({player_score: this.state.player_score + 1});
    }
    else if(player === 1){
      this.setState({computer_score: this.state.computer_score + 1});
    }
  }

  resetScore = () => {
    this.setState({player_score: 0, computer_score: 0});
  }

  render() {
    return(
      <>
        <header>
          <h1>Rock Paper Scissors</h1>
        </header>
        <div className="main">
          <div classname="score-board">
              <Score player_score={this.state.player_score} computer_score={this.state.computer_score} />
          </div>
          {(this.state.started === false)
          ? <Button className="startButton" variant="contained" onClick={this.startGame}>Start</Button>
          : <RunningGame changeGameState={this.changeGameState} setScore={this.setScore} stopGame={this.stopGame}/>
          }
        </div>
      </>
    )
  }
}

export default App;

import React from "react";
import './Score.css';

class Score extends React.Component{
  render() {
    return(
      <>
       <div class="scores">
            <div class="player">
                <p class="name">Player</p>
                <p>{this.props.player_score}</p>
            </div>
            <div class="computer">
                <p class="name">Computer</p>
                <p>{this.props.computer_score}</p>
            </div>
        </div>
      </>
    )
  }
}

export default Score;
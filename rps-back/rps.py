import numpy as np
from classfication_model import RPSClassifier
from constants import labels_to_text


class RockPaperScissors:
    def __init__(self):
        self.classifier = RPSClassifier()

    def play(self, player_choice_image: np.ndarray):
        computer_choice = np.random.choice(list(labels_to_text.values()))
        player_choice = labels_to_text[
            np.argmax(self.classifier.predict(player_choice_image))
        ]
        winner = self.get_winner(computer_choice, player_choice)
        comment = self.generate_comment(computer_choice, player_choice, winner)
        return {
            "player_choice": player_choice,
            "computer_choice": computer_choice,
            "winner": winner,
            "comment": comment,
        }

    @staticmethod
    def get_winner(computer_choice: str, player_choice: str) -> int:
        # player: 0 / computer: 1
        if player_choice == "Rock":
            if computer_choice == "Paper":
                return 1
            return 0
        elif player_choice == "Paper":
            if computer_choice == "Scissors":
                return 1
            return 0
        else:
            if computer_choice == "Rock":
                return 1
            return 0

    @staticmethod
    def generate_comment(computer_choice, player_choice, winner) -> str:
        winner = "You" if winner == 0 else "Computer"
        return f"You played {player_choice} and Computer player {computer_choice}: {winner} wins"

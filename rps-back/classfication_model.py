import numpy as np
import tensorflow as tf
from keras.layers import Conv2D, Dense, Dropout, Flatten, MaxPooling2D
from keras.models import Sequential, load_model


class RPSClassifier:
    def __init__(self):
        self.session = tf.Session()
        self.graph = tf.get_default_graph()
        self.model = self.build_model()

    def build_model(self):
        with self.graph.as_default():
            with self.session.as_default():
                return load_model("model.hdf5")

    def predict(self, x: np.ndarray) -> np.ndarray:
        with self.graph.as_default():
            with self.session.as_default():
                return self.model.predict(x)

    @staticmethod
    def build_raw_model():
        model = Sequential()
        model.add(Conv2D(16, (5, 5), activation="relu"))
        model.add(MaxPooling2D())
        model.add(Conv2D(32, (5, 5), activation="relu"))
        model.add(MaxPooling2D())
        model.add(Dropout(0.3))
        model.add(Conv2D(64, (5, 5), activation="relu"))
        model.add(MaxPooling2D())
        model.add(Dropout(0.3))
        model.add(Flatten())
        model.add(Dense(128, activation="relu"))
        model.add(Dropout(0.3))
        model.add(Dense(3, activation="softmax"))
        return model

import base64
import io

import numpy as np
from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
from PIL import Image
from rps import RockPaperScissors

app = Flask(__name__)
CORS(app)
RPS = RockPaperScissors()


@app.route("/rps_api/play", methods=["POST"])
def play():
    raw_image = str(request.data).split(",")[1]
    base64_decoded = base64.b64decode(raw_image)
    image = Image.open(io.BytesIO(base64_decoded)).convert("RGB").resize((300, 300))
    image_np = np.array(image).reshape((1, 300, 300, 3))
    print(image_np.shape)
    game_result = RPS.play(image_np)
    return jsonify(game_result)


if __name__ == "__main__":
    app.run()

Flask==1.1.1
Flask-Cors==3.0.8
h5py==2.10.0
isort==4.3.21
Keras==2.3.1
numpy==1.18.1
Pillow==7.0.0
pre-commit==2.10.0
tensorflow==1.15.0


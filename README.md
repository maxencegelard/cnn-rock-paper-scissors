# CNN Rock Paper Scissors

## Presentation

This project aims at building a Rock Paper Scissors game between a player (you) and the computer.
The addition is that the player's choice is recognized using the Webcame and a CNN for the prediction (rock, paper or scissors).

## Technology used

Front-end: ReactJS
Back-end: Flask & Tensorflow/Keras


## How to install

Requirements: Python (>3.7) and npm (>6.13)

Back-end: `cd rps-back && pip install -r requirements.txt`
Front-end: `cd rps-front && npm install`

## How to run

`cd rps-back && python server.py`

`cd rps-front && npm start`


## TODOs:

- Enhance the CNN model

# Credit

Maxence Gélard

